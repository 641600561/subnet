import java.util.Scanner;

public class SubnetCal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input IP Address (e.g., 192.168.1.10): ");
        String ipAddress = scanner.nextLine();

        System.out.print("Input Subnet Bits (e.g., 24): ");
        int subnetBits = scanner.nextInt();

        long ip = ipToLong(ipAddress);
        long subnetMask = calculateSubnetMask(subnetBits);
        long subnetID = ip & subnetMask;
        long firstHostAddress = subnetID + 1;
        long lastHostAddress = (subnetID | ~subnetMask) - 1;
        long broadcastAddress = subnetID | ~subnetMask;
        long wildcardMask = ~subnetMask;
        int maskBits = 32 - subnetBits;
        int maxSubnets = 1 << maskBits;
        int hostsPerSubnet = (int) Math.pow(2, 32 - subnetBits) - 2; // Subtract 2 for network and broadcast addresses
        String hexIPAddress = Long.toHexString(ip);
        String subnetBitmap = Long.toBinaryString(subnetMask);
        String firstOctetRange = calculateFirstOctetRange(ip, subnetMask);

        System.out.println("IP Address: " + ipAddress);
        System.out.println("Hex IP Address: " + hexIPAddress);
        System.out.println("Subnet Mask: " + longToIp(subnetMask));
        System.out.println("Subnet Bits: " + subnetBits);
        System.out.println("Maximum Subnets: " + maxSubnets);
        System.out.println("Hosts per Subnet: " + hostsPerSubnet);
        System.out.println("Subnet ID: " + longToIp(subnetID));
        System.out.println("Host Address Range: " + longToIp(firstHostAddress) + " - " + longToIp(lastHostAddress));
        System.out.println("Broadcast Address: " + longToIp(broadcastAddress));
        System.out.println("Wildcard Mask: " + longToIp(wildcardMask));
        System.out.println("Subnet Bitmap: " + subnetBitmap);
        System.out.println("Mask Bits: " + maskBits);
        System.out.println("First Octet Range: " + firstOctetRange);
    }

    public static long ipToLong(String ipAddress) {
        String[] octets = ipAddress.split("\\.");
        long result = 0;
        for (int i = 0; i < octets.length; i++) {
            result += Long.parseLong(octets[i]) << (24 - 8 * i);
        }
        return result;
    }

    public static String longToIp(long ip) {
        return String.format("%d.%d.%d.%d", (ip >> 24) & 0xFF, (ip >> 16) & 0xFF, (ip >> 8) & 0xFF, ip & 0xFF);
    }

    public static long calculateSubnetMask(int subnetBits) {
        if (subnetBits >= 0 && subnetBits <= 32) {
            return (0xFFFFFFFF << (32 - subnetBits)) & 0xFFFFFFFF;
        } else {
            return 0;
        }
    }

    public static String calculateFirstOctetRange(long ip, long mask) {
        long firstOctet = (ip & mask) >> 24;
        if (firstOctet >= 1 && firstOctet <= 126) {
            return "1 - 126";
        } else if (firstOctet >= 128 && firstOctet <= 191) {
            return "128 - 191";
        } else if (firstOctet >= 192 && firstOctet <= 223) {
            return "192 - 223";
        } else if (firstOctet >= 224 && firstOctet <= 239) {
            return "Multicast";
        } else if (firstOctet >= 240 && firstOctet <= 255) {
            return "Reserved";
        } else {
            return "Not in the defined IP address classes";
        }
    }
}